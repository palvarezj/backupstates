import json
import requests

class JsonData:
	"""
	Used to load file into a variable, without downloading it.
	"""
	data = dict()
	read_bytes = 0

	def get_data_bytes(self):
		return json.dumps(self.data, indent=4).encode("utf-8")

	def read(self, size=None):
		if size:
			bytes = self.get_data_bytes()
			start = self.read_bytes
			end = min(len(bytes), size)
			self.read_bytes = end
			return bytes[start:end]
		self.read_bytes = 0
		return b""

	def write(self, data):
		self.data = json.loads(data)

	def get(self, key, default=None):
		return self.data.get(key, default)

def download_file_from_url(file_url, file_name, headers=None):
	"""
	Downloads file from the given url.

	:param file_url: URL to be downloading from
	:type file_url: string
	:param file_name: Path to download to
	:type file_name: string
	:param headers: Headers for the request
	:type headers: dict
	"""
	response = requests.get(file_url, headers=headers)
	open(file_name, "wb").write(response.content)