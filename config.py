import os
import logging

ENV_VARS = os.environ
TOKEN = ENV_VARS.get("TF_CLOUD_TOKEN", "")
ORGANIZATION = ENV_VARS.get("TF_CLOUD_ORGANIZATION", "Ripley-Digital-Factory")
AWS_KEY = ENV_VARS.get("AWS_ACCESS_KEY_ID", "")
AWS_SECRET = ENV_VARS.get("AWS_SECRET_ACCESS_KEY", "")
AWS_REGION = ENV_VARS.get("AWS_REGION", "us-east-1")
LOGGING_LEVEL = ENV_VARS.get("LOGGING_LEVEL", "INFO")
BUCKET_BACKUP = "terraform-remotes-states"
INFO_FILENAME = "backup-info.json"
BUCKET_KEY = "backup"


levels = {
	"CRITICAL": logging.CRITICAL,
	"ERROR": logging.ERROR,
	"WARNING": logging.WARNING,
	"INFO": logging.INFO,
	"DEBUG": logging.DEBUG,
}

logging.basicConfig(
	format="[%(levelname)s] %(asctime)s %(message)s",
	level=levels[LOGGING_LEVEL]
)