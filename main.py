from terraform_cloud import TerraformCloud
from aws_provider import AWSProvider
from utils import JsonData, download_file_from_url
from config import *

import os

def main(*args, **kwargs):
	aws = AWSProvider(
		aws_access_key_id=AWS_KEY,
		aws_secret_access_key=AWS_SECRET,
		region=AWS_REGION,
	)
	tf_cloud = TerraformCloud(
		token=TOKEN,
		organization=ORGANIZATION,
	)
	changes = False

	# Get backup info data
	try:
		states_info = aws.s3_load_json_file(BUCKET_BACKUP, INFO_FILENAME)
	except Exception as e:
		if e.response["Error"]["Code"] == "404":
			# When file does not exist, it will be created
			states_info = JsonData()
		else:
			logging.error(
				"[ERROR] Accessing %s/%s with error: %s",
				BUCKET_BACKUP, INFO_FILENAME, e
			)
			exit()

	workspaces = tf_cloud.get_workspaces({
		"fields[workspaces][]": "name",
		"page[size]": "100",
	})
	for workspace in workspaces:
		ws_name = workspace["attributes"]["name"]
		ws_id = workspace["id"]
		ws_state = tf_cloud.get_current_state(ws_id)
		logging.debug(
			"Computing id %s (%s) has state %s",
			ws_id, ws_name, ws_state is not None
		)
		if ws_state is None:
			continue

		# If current state version is outdated, upload new version
		if ws_state["id"] != states_info.get(ws_id, {}).get("state-version-id"):
			changes = True
			states_info.data[ws_id] = {
				"state-version-id": ws_state["id"],
				"state-url": ws_state["url"],
				"name": ws_name,
			}
			file_path = ws_id + ".tfstate"
			try:
				download_file_from_url(ws_state["url"], file_path)
				aws.s3_upload_file(file_path, BUCKET_BACKUP, f"{BUCKET_KEY}/{ws_id}.json")
				os.remove(file_path)
				logging.info("[UPLOAD] %s", file_path)
			except Exception as e:
				logging.error("[ERROR] Backuping %s with error: %s", ws_name, e)
	
	# Upload new state info file if changed
	if changes:
		aws.s3_upload_json_file(states_info, BUCKET_BACKUP, INFO_FILENAME)
		logging.info("[UPDATE] States info file")

if __name__ == "__main__":
	main()