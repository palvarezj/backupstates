import json
import requests


class TerraformCloud:
	_base_url = "https://app.terraform.io/api/v2"

	def __init__(
		self,
		token="my-token",
		organization="my-org"):
		"""
		Sets headers to be used when calling TerraformCloud API.

		:param token: Token API for TerraformCloud
		:type token: string
		:param organization: Organization name
		:type organization: string
		"""
		self._tf_cloud_token = token
		self._tf_cloud_organization = organization
		self._workspaces_url = f"{self._base_url}/organizations/{organization}/workspaces"

		self._headers = {
			"Authorization": "Bearer " + token,
			"Content-Type": "application/vnd.api+json",
		}

	def get_workspaces(self, fields=None):
		"""
		Get all workspaces.

		:return: Workspace's variables
		:type return: list(dict)
		:raise Exception: When error in fetching workspaces
		"""
		workspaces = []
		url = self._workspaces_url
		while True:
			response = requests.get(
				url,
				params=fields,
				headers=self._headers)
			if response.status_code != 200:
				raise Exception(f"Error fetching workspaces {response.status_code}: {response.text}")
			workspaces += response.json()["data"]
			url = response.json().get("links", {}).get("next")
			if url is None:
				break
		return workspaces

	def get_current_state(self, workspace_id):
		"""
		Download current state of given workspace.

		:param workspace_id: Id of the workspace to fetch state.
		:type workspace_id: string
		:return: URL where the state can be downloaded from and the state
			version id
		:type return: dict
		:raise Exception: When error in fetching state
		"""
		response = requests.get(
			f"{self._base_url}/workspaces/{workspace_id}/current-state-version?fields[state-versions][]=hosted_state_download_url",
			headers=self._headers)
		if response.status_code == 404:
			return None
		elif response.status_code != 200:
			raise Exception(f"Error fetching workspace state {response.status_code}: {response.text}")
		data = response.json()["data"]
		return {
			"id": data["id"],
			"url": data["attributes"]["hosted-state-download-url"]
		}


