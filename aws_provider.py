import boto3
from utils import JsonData

class AWSProvider:
	def __init__(
		self,
		aws_access_key_id=None,
		aws_secret_access_key=None,
		aws_session_token=None,
		region=None,
		profile_name=None):
		"""
		Creates the session to connect to AWS and
		the client for S3.

		:param aws_access_key_id: AWS access key ID
		:type aws_access_key_id: string
		:param aws_secret_access_key: AWS secret access key
		:type aws_secret_access_key: string
		:param aws_session_token: AWS temporary session token
		:type aws_session_token: string
		:param region: Default region when creating new connections
		:type region: string
		:param profile_name: The name of a profile to use. If not given,
							 then the default profile is used.
		:type profile_name: string
		"""
		self._session = boto3.session.Session(
			aws_access_key_id=aws_access_key_id,
			aws_secret_access_key=aws_secret_access_key,
			aws_session_token=aws_session_token,
			region_name=region,
			profile_name=profile_name
		)
		self._s3_client = self._session.client(
			"s3",
			region,
			endpoint_url="https://s3.us-east-1.amazonaws.com",
		)

	def list_buckets(self):
		"""
		Lists all existing buckets in the account.

		:return: All buckets as dictionaries, with its name and
			creation date as datetime
		:type return: list(dict)
		"""
		response = self._s3_client.list_buckets()
		return response["Buckets"]

	def list_objects(self, bucket):
		"""
		Lists all objects in given bucket.

		:param bucket: The name of the bucket to upload to
		:type bucket: string
		:return: All objects as dictionaries
		:type return: list(dict)
		"""
		objects = []
		continuation_token = None
		while True:
			response = self._s3_client.list_objects_v2(
				Bucket=bucket,
				ContinuationToken=continuation_token,
			)
			objects += response["Contents"]
			if not response["isTruncated"]:
				break
			continuation_token = response["NextContinuationToken"]
		return objects

	def create_bucket(self, name):
		"""
		Create bucket in default region (us-east-1)

		:param name: Bucket to create
		:type name: string
		"""
		try:
			self._s3_client.create_bucket(
				Bucket=name,
			)
			return True
		except Exception as e:
			raise e

	def s3_upload_file(self, file_path, bucket, key):
		"""
		Upload file to bucket.

		:param file_path: The path to the file to upload
		:type file_path: string
		:param bucket: The name of the bucket to upload to
		:type bucket: string
		:param key: The name of the key to upload to
		:type key: string
		"""
		self._s3_client.upload_file(file_path, bucket, key)


	def s3_upload_json_file(self, data, bucket, file_name):
		"""
		Upload json data as file to bucket.

		:param data: The data object to be uploaded
		:type data: JsonData
		:param bucket: The name of the bucket to upload to
		:type bucket: string
		:param file_name: The name of the key to upload to
		:type file_name: string
		"""
		self._s3_client.upload_fileobj(data, bucket, file_name)

	def s3_download_file(self, bucket, key, file_name):
		"""
		Download file from bucket.

		:param bucket: The name of the bucket to download from
		:type bucket: string
		:param key: The name of the key to download from
		:type key: string
		:param file_name: The path to the file to download to
		:type file_name: string
		"""
		self._s3_client.download_file(bucket, key, file_name)

	def s3_load_json_file(self, bucket, file_name):
		"""
		Load JSON file. 

		:param bucket: The name of the bucket where the file is found
		:type bucket: string
		:param file_name: The name of the key of the file
		:type file_name: string
		:return: Data in the json file
		:return type: dict
		"""
		data = JsonData()
		self._s3_client.download_fileobj(bucket, file_name, data)
		return data