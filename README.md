# Backup Terraform States
This software backups the current terraform state of all workspaces associated to the given organization. It stores the states in the AWS S3 bucket called `terraform-remotes-states` in the folder `backup`.

It also creates and updates the file `backup-info.json` to the said bucket, which contains the backup information in the format:
```
{
	<workspace-id>: {
		state-version-id: <sv-id>,
		state-url: <archivist-state-url>,
		name: <workspace-name>
	},
	...
}
```
Such file is used to avoid re-uploading files that have already been backuped. 

## Usage

### Requirements
- This code was tested with `Python 3.8.5` and `pip 20.1.1`.
- You need a Terraform Cloud account, with read access to workspaces and their states.
- You need programmatic access to AWS, with access to the `terraform-remotes-states` S3 bucket, with read and write permissions.
- The AWS S3 bucket `terraform-remotes-states` with the folder `backup` must exist.

#### Environment variables
It is required to define the following variables in the environment.

- **TF_CLOUD_TOKEN**: Terraform Cloud API token. Check [how to generate a token](https://learn.hashicorp.com/tutorials/terraform/cloud-login#generate-a-token).
- **TF_CLOUD_ORGANIZATION**: Organization name where the workspaces are found. Default value: `Ripley-Digital-Factory`.
- **AWS_ACCESS_KEY_ID**: AWS access key id. Check [how to get AWS programmatic access](https://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html).
- **AWS_SECRET_ACCESS_KEY**:  AWS secret access key. Check [how to get AWS programmatic access](https://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html).
- **AWS_REGION**: AWS region where S3 bucket is found. Default value: `us-east-1`.
- **LOGGING_LEVEL**: Level for logging. Default value: `INFO`.

#### Installing packages
Run the following command:
```
pip install -r requirements.txt --no-index 
```

###  Running
Run the following command:
```
python main.py 
```